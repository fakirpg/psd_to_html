jQuery(document).ready(function ($) {	
	
	$(function(){
		$('#nav').slicknav();
	});
	
	$(".testimonial_content").owlCarousel({
		items : 1,
		itemsDesktop : [1170,1],
		itemsDesktopSmall : [970,1],
		itemsTablet: [768,1],
		itemsMobile : [480,1],
		singleItem : false,
		itemsScaleUp : false,
		autoPlay : true,
	});
	
});

